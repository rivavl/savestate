package com.marina.activities.entity

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PersonInfo(
    val name: String,
    val surname: String,
    val patronymic: String,
    val age: Int,
    val hobby: String?
) : Parcelable