package com.marina.activities.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.marina.activities.R
import com.marina.activities.activity.InfoActivity.Companion.PERSON
import com.marina.activities.databinding.ActivityMainBinding
import com.marina.activities.entity.PersonInfo
import com.marina.activities.view_model.MainViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this)[MainViewModel::class.java]

        binding.btnGoToThirdScreen.setOnClickListener {
            //если все обязательные поля(все, кроме хобби) заполнены, переходим в след активити
            if (validateTextFields()) {
                goToThirdActivity()
            }
        }
        observeViewModel()
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private fun observeViewModel() {
        viewModel.imageName.observe(this) {
            binding.root.background = getDrawable(it)
        }
    }

    //проверяем, что обязательные поля не пустые
    private fun validateTextFields(): Boolean {
        val name: String = binding.nameInputText.text.toString()
        val surname: String = binding.surnameInputText.text.toString()
        val patronymic: String = binding.fatherNameInputText.text.toString()
        val age: Int? = binding.ageInputText.text.toString().toIntOrNull()

        if (name.isEmpty() || surname.isEmpty() || patronymic.isEmpty() || age == null) {
            showSnackbar(getString(R.string.fill_all_fields))
            return false
        }
        return true
    }

    //создаем интент и стартуем активити
    private fun goToThirdActivity() {
        val name: String = binding.nameInputText.text.toString()
        val surname: String = binding.surnameInputText.text.toString()
        val patronymic: String = binding.fatherNameInputText.text.toString()
        val age: Int = binding.ageInputText.text.toString().toInt()
        val hobby: String = binding.hobbyInputText.text.toString()

        val intent = Intent(this, InfoActivity::class.java)
        intent.putExtra(
            PERSON,
            PersonInfo(
                name = name,
                surname = surname,
                patronymic = patronymic,
                age = age,
                hobby = hobby
            )
        )
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.shared_prefs -> saveWithSharedPrefs()
            R.id.background_photo -> changeBGImage()
            R.id.go_to_new_activity -> goToNewActivityAndUseViewModel()
        }
        return true
    }

    private fun changeBGImage() {
        viewModel.getNewImage()
    }

    //сохранение состояния полей Shared Preferences
    private fun saveWithSharedPrefs() {
        if (validateTextFields()) {
            val name: String = binding.nameInputText.text.toString()
            val surname: String = binding.surnameInputText.text.toString()
            val patronymic: String = binding.fatherNameInputText.text.toString()
            val age: Int = binding.ageInputText.text.toString().toInt()
            val hobby: String = binding.hobbyInputText.text.toString()

            viewModel.saveWithSP(
                this,
                PersonInfo(
                    name = name,
                    surname = surname,
                    patronymic = patronymic,
                    age = age,
                    hobby = hobby
                )
            )
        }
    }

    //Запуск новой активити с текстовым полем и кнопкой
    private fun goToNewActivityAndUseViewModel() {
        val intent = Intent(this, NewActivity::class.java)
        startActivity(intent)
    }

    private fun showSnackbar(info: String) {
        Snackbar.make(
            binding.root,
            info,
            Snackbar.LENGTH_LONG
        ).setAnchorView(binding.btnGoToThirdScreen).show()
    }
}
