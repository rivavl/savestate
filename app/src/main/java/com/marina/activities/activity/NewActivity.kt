package com.marina.activities.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import com.marina.activities.R
import com.marina.activities.databinding.ActivityNewBinding
import com.marina.activities.view_model.NewActivityViewModel

class NewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNewBinding
    private lateinit var viewModel: NewActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewBinding.inflate(layoutInflater)
        setContentView(binding.root)
        viewModel = ViewModelProvider(this)[NewActivityViewModel::class.java]

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        observeViewModel()
    }

    //переопределяем действия при нажатии на кнопку назад
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    private fun observeViewModel() {
        viewModel.personInfo.observe(this) {
            with(binding) {
                tvName.text = it.name
                tvSurname.text = it.surname
                tvAge.text = it.age.toString()
                tvPatronymic.text = it.patronymic
            }
            setHobby(it.hobby)
        }
        binding.newSaveBtn.setOnClickListener {
            viewModel.loadFromSP(this)
        }

    }

    fun setHobby(hobby: String?) {
        if (hobby?.isNotEmpty() == true) {
            binding.tvHobby.isVisible = true
            binding.tvHobby.text = hobby
            binding.hobby.isVisible = true
        }
    }
}
