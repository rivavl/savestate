package com.marina.activities.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.marina.activities.*
import com.marina.activities.entity.PersonInfo
import kotlin.random.Random

class MainViewModel : ViewModel() {

    private var _imageName = MutableLiveData<Int>()
    val imageName: LiveData<Int> get() = _imageName

    init {
        _imageName.value = R.drawable.capi7
    }

    fun getNewImage() {
        val name = getRandomImageID()
        _imageName.postValue(name)
    }

    private fun getRandomImageID(): Int {
        return IMAGES[Random.nextInt(0, IMAGES.lastIndex)]
    }

    fun saveWithSP(context: Context, personInfo: PersonInfo) {
        val sharedPrefs = context.getSharedPreferences(SH_PREFS, Context.MODE_PRIVATE)
        val editor = sharedPrefs.edit()

        val name = personInfo.name
        val surname = personInfo.surname
        val patronymic = personInfo.patronymic
        val age = personInfo.age
        val hobby = personInfo.hobby

        editor.apply {
            putString(NAME, name)
            putString(SURNAME, surname)
            putString(PATRONYMIC, patronymic)
            putInt(AGE, age)
            putString(HOBBY, hobby)
            apply()
        }
    }

    companion object {
        private val IMAGES =
            mutableListOf(
                R.drawable.capi10,
                R.drawable.capi11,
                R.drawable.capi12,
                R.drawable.capi9,
                R.drawable.capi8,
                R.drawable.capi7,
                R.drawable.capi6,
                R.drawable.capi5,
                R.drawable.capi4
            )
    }
}