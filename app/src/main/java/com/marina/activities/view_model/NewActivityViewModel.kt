package com.marina.activities.view_model

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.marina.activities.*
import com.marina.activities.entity.PersonInfo

class NewActivityViewModel : ViewModel() {

    private val _text = MutableLiveData<String>()
    val text: LiveData<String> get() = _text

    private val _personInfo = MutableLiveData<PersonInfo>()
    val personInfo: LiveData<PersonInfo> get() = _personInfo

    fun saveText(text: String) {
        _text.value = text
    }

    fun loadFromSP(context: Context) {
        val sharedPrefs = context.getSharedPreferences(SH_PREFS, Context.MODE_PRIVATE)

        val name = sharedPrefs.getString(NAME, null) ?: throw IllegalStateException()
        val surname = sharedPrefs.getString(SURNAME, null) ?: throw IllegalStateException()
        val patronymic = sharedPrefs.getString(PATRONYMIC, null) ?: throw IllegalStateException()
        val age = sharedPrefs.getInt(AGE, 0)
        val hobby = sharedPrefs.getString(HOBBY, null)

        _personInfo.postValue(
            PersonInfo(
                name = name,
                surname = surname,
                patronymic = patronymic,
                age = age,
                hobby = hobby
            )
        )
    }
}