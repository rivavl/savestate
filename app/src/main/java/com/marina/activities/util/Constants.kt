package com.marina.activities

const val SH_PREFS = "sh_p"

const val NAME: String = "name"
const val SURNAME: String = "surname"
const val PATRONYMIC: String = "patronymic"
const val AGE: String = "age"
const val HOBBY: String = "hobby"
